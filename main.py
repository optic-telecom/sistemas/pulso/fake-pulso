from fastapi import FastAPI, Request
import asyncio
import json
import sys
from starlette.requests import Request
from starlette.responses import Response

app = FastAPI()


@app.get("/api/v1/search-location/")
async def read_root(request: Request):
    data = {
        "intento": "Exitoso",
        "street_location": 12345
    }
    
    return data

@app.get("/api/v1/complete-location/{complete_location_id}/")
async def read_root(complete_location_id: int, request: Request):
    data = {
        "intento": "Exitoso",
        "street_location": 54321
    }

    return data